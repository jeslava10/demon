package com.empresa.demo.controller;


import com.empresa.demo.model.Students;
import com.empresa.demo.service.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentsController {

    private final StudentsService studentsService;

    @Autowired
    public StudentsController(StudentsService studentsService) {
        this.studentsService = studentsService;
    }

    @PostMapping(path = "/save")
    public Students saveStudent (@RequestBody Students students){
        return studentsService.saveStudents(students);
    }

    @PutMapping(path = "/update")
    public Students updateStudent (@RequestBody Students students){
        return studentsService.updateStudents(students);
    }

    @GetMapping(path = "/all/name")
    public List<Students> finfAllByFristName (@RequestParam(name = "name") String fristName){

        return studentsService.finfAllByFristName(fristName);
    }

    @GetMapping(path = "/number-id")
    public Students findOneByNumberId (@RequestParam(name = "number") Integer numberId){

        return studentsService.findOneByNumberId(numberId);
    }

    @DeleteMapping(path = "/delete")
    public void deleteByID (@RequestParam(name = "id") Long id){

        studentsService.deleteStudents(id);
    }


}
