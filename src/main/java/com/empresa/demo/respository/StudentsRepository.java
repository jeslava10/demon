package com.empresa.demo.respository;

import com.empresa.demo.model.Students;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentsRepository extends JpaRepository<Students, Long> {

    List<Students> findAllByStFirstName(String firstName);

    @Query(nativeQuery = true,
    value = "SELECT" +
            " st_id," +
            " st_first_name," +
            " st_second_name," +
            " st_first_surname," +
            " st_second_surname," +
            " st_number_id," +
            " st_active" +
            "FROM students" +
            " Where st_number_id = :dni")
    Students getOneByNumberId (@Param(value = "dni") Integer numberId);
}