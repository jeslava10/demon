package com.empresa.demo.service;

import com.empresa.demo.model.Students;
import com.empresa.demo.respository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentsService {

    private final StudentsRepository studentsRepository;

    @Autowired
    public  StudentsService(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    public Students saveStudents(Students students) {
        return studentsRepository.save(students);
    }

    public Students updateStudents(Students students) {
        return studentsRepository.save(students);
    }

    public List<Students> finfAllByFristName(String firstName){

        return studentsRepository.findAllByStFirstName(firstName);

    }

    public Students findOneByNumberId(Integer dni){

        return studentsRepository.getOneByNumberId(dni);

    }

    public  void deleteStudents(Long id) {
          studentsRepository.deleteById(id);
    }






}
