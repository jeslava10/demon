package com.empresa.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "students")
public class Students {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "st_id")
    private Long stId;

    @Column(name = "st_first_name")
    private String stFirstName;

    @Column(name = "st_second_name")
    private String stSecondName;

    @Column(name = "st_second_surname")
    private String stSecondSurname;

    @Column(name = "st_first_surname")
    private String stFirstSurname;

    @Column(name = "st_number_id")
    private Integer stNumberId;

    @Column(name = "st_active")
    private Boolean stActive;

    public Students(Long stId, String stFirstName, String stSecondName, String stSecondSurname, String stFirstSurname, Integer stNumberId, Boolean stActive) {
        this.stId = stId;
        this.stFirstName = stFirstName;
        this.stSecondName = stSecondName;
        this.stSecondSurname = stSecondSurname;
        this.stFirstSurname = stFirstSurname;
        this.stNumberId = stNumberId;
        this.stActive = stActive;
    }

    public void setStId(Long stId) {
        this.stId = stId;
    }

    public void setStFirstName(String stFirstName) {
        this.stFirstName = stFirstName;
    }

    public void setStSecondName(String stSecondName) {
        this.stSecondName = stSecondName;
    }

    public void setStSecondSurname(String stSecondSurname) {
        this.stSecondSurname = stSecondSurname;
    }

    public void setStFirstSurname(String stFirstSurname) {
        this.stFirstSurname = stFirstSurname;
    }

    public void setStNumberId(Integer stNumberId) {
        this.stNumberId = stNumberId;
    }

    public void setStActive(Boolean stActive) {
        this.stActive = stActive;
    }

    public Long getStId() {
        return stId;
    }

    public String getStFirstName() {
        return stFirstName;
    }

    public String getStSecondName() {
        return stSecondName;
    }

    public String getStSecondSurname() {
        return stSecondSurname;
    }

    public String getStFirstSurname() {
        return stFirstSurname;
    }

    public Integer getStNumberId() {
        return stNumberId;
    }

    public Boolean getStActive() {
        return stActive;
    }
}
